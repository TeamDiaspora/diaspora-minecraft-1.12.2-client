import crafttweaker.item.IItemStack;
import crafttweaker.item.IItemCondition;
import crafttweaker.item.IIngredient;
import crafttweaker.data.IData;
import mods.zenstages.Utils;

import mods.thaumcraft.Infusion as Infusion;
import mods.thaumcraft.SmeltingBonus as Furnace;

import mods.thaumicwands.WandCaps;
import mods.thaumicwands.WandRods;

import scripts.crafttweaker.staticstages.MainStage_Thaumcraft;
import scripts.crafttweaker.staticstages.SubStage_Abyssal;

/*
    Recipe Removals
*/
static RecipesToRemove as IItemStack[] = [

];

static FurnaceRecipesToRemove as IIngredient[] = [

];

/*
    Shaped Recipes
*/
static ShapedRecipes as IIngredient[][][][IItemStack] = {
    <thermalfoundation:tome_lexicon>:
    [
        [
            [<thaumcraft:crystal_essence>.withTag({Aspects: [{amount: 1, key: "potentia"}]}), null, <thaumcraft:crystal_essence>.withTag({Aspects: [{amount: 1, key: "metallum"}]})],
            [null, <minecraft:book>, null],
            [<thaumcraft:crystal_essence>.withTag({Aspects: [{amount: 1, key: "volatus"}]}), <thaumcraft:curio:*>, <thaumcraft:crystal_essence>.withTag({Aspects: [{amount: 1, key: "perditio"}]})]
        ]
    ]
};

static HiddenShapedRecipes as IIngredient[][][][IItemStack] = {

};


/*
    Mirrored Recipes
*/
static MirroredRecipes as IIngredient[][][][IItemStack] = {

};

/*
    Shapeless Recipes
*/
static ShapelessRecipes as IIngredient[][][IItemStack] = {

};

static HiddenShapelessRecipes as IIngredient[][][IItemStack] = {

};

static ShadowCreatureEgg as IItemStack = <minecraft:spawn_egg>.withTag({EntityTag: {id: "abyssalcraft:shadowcreature"}});
function Init() {
    Util.RemoveRecipes(RecipesToRemove);
	Util.RemoveFurnace(FurnaceRecipesToRemove);
	Util.AddShaped(ShapedRecipes, false, false);
    Util.AddShaped(HiddenShapedRecipes, false, true);
    Util.AddShaped(MirroredRecipes, true, false);
    Util.AddShapeless(ShapelessRecipes, false);
    Util.AddShapeless(HiddenShapelessRecipes, true);

    // New infusions
    Infusion.registerRecipe("ShadowCreatureEgg", "", ShadowCreatureEgg, 80, [<aspect:ignis> * 100, <aspect:motus> * 100, <aspect:alienis> * 100, <aspect:vitium> * 100], <minecraft:egg>, [<thaumcraft:void_seed>, <thaumcraft:brain>, <minecraft:rotten_flesh>, <minecraft:rotten_flesh>]);

    // Extra smelting bonuses
    Furnace.addSmeltingBonus(<minecraft:wheat_seeds>, <minecraft:nether_wart> % 2);
    Furnace.addSmeltingBonus(<minecraft:wheat_seeds>, <dungeontactics:coal_tinydust> % 10);

    // Extra wands
    WandRods.register("knightslime", "FIRSTSTEPS", 150, <tconstruct:tough_tool_rod>.withTag({Material: "knightslime"}), 15);
    game.setLocalization("item.wand.knightslime.rod","Knightslime Rod");

    // Wand recharge recipe
    recipes.addShapeless(
        // Turn the vis to dust
        <dungeontactics:coal_tinydust>,

        // Output the wand with it's charge increased up to a maximum of 50
        [<thaumicwands:item_wand>.transformNew(function(item) {
            var tags = item.tag;
            var chargeMember = tags.memberGet("tc.charge");
            var charge as int = 0;

            if (!isNull(chargeMember))
            {
                charge = chargeMember.asInt();
            }

            if (charge < 50)
            {
                charge += 1;
            }

            var newTags as IData = {"tc.charge": charge};
            return item.updateTag(newTags);
        }),

        // Input any vis crystal with the wand
        <thaumcraft:crystal_essence>]
    );
}
