#reloadable
#loader contenttweaker

import mods.contenttweaker.Color;
import mods.contenttweaker.Material;
import mods.contenttweaker.MaterialSystem;

static materials as Material[string] = {
	"hslasteel": MaterialSystem.getMaterialBuilder().setName("HSLA Steel").setColor(Color.fromHex("5b5a66")).build(),
};

// register parts
var HSLASteelParts as string[] = [
    "block",
    "ingot",
    "nugget",
    "dust",
    "gear",
    "plate",
    "rod"
];
materials.hslasteel.registerParts(HSLASteelParts);