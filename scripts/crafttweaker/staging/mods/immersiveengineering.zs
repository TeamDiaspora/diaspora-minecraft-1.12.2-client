import crafttweaker.item.IIngredient;
import mods.zenstages.ZenStager;

import scripts.crafttweaker.staticstages.MainStage_Settling;
import scripts.crafttweaker.staticstages.Stage_Disabled;

static StagedItems as IIngredient[][string] = {

};

static StagedItemOverrides as IIngredient[][string] = {
    MainStage_Settling.stage:
    [
        <immersiveengineering:seed>,
        <immersiveengineering:material:4>
    ],

    Stage_Disabled.stage: [
        <immersiveengineering:sword_steel>,
        <immersiveengineering:axe_steel>,
        <immersiveengineering:hoe_steel>,
        <immersiveengineering:shovel_steel>,
        <immersiveengineering:pickaxe_steel>,
        <immersiveengineering:steel_armor_chest>,
        <immersiveengineering:steel_armor_head>,
        <immersiveengineering:steel_armor_legs>,
        <immersiveengineering:steel_armor_feet>,
    ]
};

function Init() {
    for StageName, Items in StagedItems {
		ZenStager.getStage(StageName).addIngredients(Items);
	}
}

function InitOverrides() {
    var ModId as string = StagedItemOverrides.entrySet[0].value[0].items[0].definition.owner;
	var ModStage as string = scripts.crafttweaker.staging.modid.ContainsMod(ModId);
    var bModWasStaged as bool = ModStage != "";

    for StageName, Items in StagedItemOverrides {
        if (bModWasStaged && StageName != ModStage)
        {
            ZenStager.addModItemOverrides(ModId, Items);
        }

        for Item in Items {
		    ZenStager.getStage(StageName).addIngredientOverride(Item);
        }
	}
}