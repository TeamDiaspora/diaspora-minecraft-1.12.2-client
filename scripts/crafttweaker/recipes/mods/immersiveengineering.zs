import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import crafttweaker.oredict.IOreDictEntry;
import crafttweaker.mods.IMod;
import crafttweaker.mods.ILoadedMods;
import crafttweaker.recipes.IFurnaceManager;
import crafttweaker.recipes.IRecipeManager;
import mods.jei.JEI;
import mods.immersiveengineering.BlastFurnace;
import mods.immersiveengineering.Blueprint;
import mods.zenstages.Utils;

/*
    Recipe Removals
*/
static RecipesToRemove as IItemStack[] = [
    <immersiveengineering:sword_steel>,
    <immersiveengineering:axe_steel>,
    <immersiveengineering:hoe_steel>,
    <immersiveengineering:shovel_steel>,
    <immersiveengineering:pickaxe_steel>,
    <immersiveengineering:steel_armor_chest>,
    <immersiveengineering:steel_armor_head>,
    <immersiveengineering:steel_armor_legs>,
    <immersiveengineering:steel_armor_feet>,
    <immersiveengineering:tool:3>,
    <immersiveengineering:metal:32>
];

static FurnaceRecipesToRemove as IIngredient[] = [
    <jsg:titanium_ore>
];

/*
    Shaped Recipes
*/
static ShapedRecipes as IIngredient[][][][IItemStack] = {

};

static HiddenShapedRecipes as IIngredient[][][][IItemStack] = {

};


/*
    Mirrored Recipes
*/
static MirroredRecipes as IIngredient[][][][IItemStack] = {

};

/*
    Shapeless Recipes
*/
static ShapelessRecipes as IIngredient[][][IItemStack] = {
    <immersiveengineering:tool:3>: [
        [<ore:ingotNaquadahRaw>, <minecraft:book>]
    ],
    <thermalfoundation:material:323>: [
        [<immersiveengineering:sheetmetal:2>],
        [<ore:ingotLead>, <immersiveengineering:tool>.reuse()]
    ],
    <contenttweaker:material_part:11>: [
        [<immersiveengineering:sheetmetal:4>],
        [<ore:ingotHSLASteel>, <immersiveengineering:tool>.reuse()]
    ]
};

static HiddenShapelessRecipes as IIngredient[][][IItemStack] = {

};

static IgnoredModItems as IItemStack[] = [
    <immersiveengineering:metal_decoration1>,
    <immersiveengineering:metal:38>,
    <immersiveengineering:material:2>,
    <immersiveengineering:stone_decoration:7>,
    <immersiveengineering:storage:2>,
    <immersiveengineering:storage:8>,
    <immersiveengineering:metal:2>,
    <immersiveengineering:metal:8>,
];

function Init() {
    // Adjust all the recipes
    Util.RemoveRecipes(RecipesToRemove);
	Util.RemoveFurnace(FurnaceRecipesToRemove);
	Util.AddShaped(ShapedRecipes, false, false);
    Util.AddShaped(HiddenShapedRecipes, false, true);
    Util.AddShaped(MirroredRecipes, true, false);
    Util.AddShapeless(ShapelessRecipes, false);
    Util.AddShapeless(HiddenShapelessRecipes, true);

    // Add Steel -> HSLA Steel blast furnace recipe
    BlastFurnace.addRecipe(<contenttweaker:material_part:7>, <dungeontactics:steel_ingot>, 1200, <immersiveengineering:material:7> * 3);

    // Update steel component recipe
    Blueprint.removeRecipe(<immersiveengineering:material:9>);
    Blueprint.addRecipe("components", <immersiveengineering:material:9>,
        [
            <contenttweaker:material_part:11>,
            <contenttweaker:material_part:11>,
            <thermalfoundation:material:128>
        ]
    );

    logger.logInfo("Building list of IE items");
    var IEMod = loadedMods["immersiveengineering"];
    var ModItems as IIngredient = <immersiveengineering:metal_decoration0:5>;
    for Item in IEMod.items {
        if (!(IgnoredModItems has Item))
        {
            logger.logInfo("Adding " ~ Item.name);
            ModItems = ModItems | Item;
        }
        else
        {
            logger.logInfo("Skipping " ~ Item.name);
        }
    }
    logger.logInfo("Finished building list of IE items");

    // Replace all Steel recipes with HSLA Steel
    recipes.replaceAllOccurences(<ore:ingotSteel>, <ore:ingotHSLASteel>, ModItems);
    recipes.replaceAllOccurences(<ore:plateSteel>, <ore:plateHSLASteel>, ModItems);
    recipes.replaceAllOccurences(<ore:blockSteel>, <ore:blockHSLASteel>, ModItems);
    recipes.replaceAllOccurences(<ore:nuggetSteel>, <ore:nuggetHSLASteel>, ModItems);
    recipes.replaceAllOccurences(<ore:stickSteel>, <ore:stickHSLASteel>, ModItems);
    recipes.replaceAllOccurences(<ore:gearSteel>, <ore:gearHSLASteel>, ModItems);
    recipes.replaceAllOccurences(<ore:ingotLead>, <ore:ingotNaquadahRaw>, ModItems);
    recipes.replaceAllOccurences(<ore:blockLead>, <ore:blockNaquadahRaw>, ModItems);
}
