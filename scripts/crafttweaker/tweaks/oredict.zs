import crafttweaker.oredict.IOreDictEntry;
import crafttweaker.oredict.IOreDict;
import crafttweaker.item.IItemStack;
import mods.storagedrawers.Compaction;

static OreDictMaterials as IOreDictEntry[string][string] = {
    "Steel": {
        "nugget": <ore:nuggetSteel>,
        "block": <ore:blockSteel>,
        "ingot": <ore:ingotSteel>,
    },
    "Mithril": {
        "nugget": <ore:nuggetMithril>,
        "block": <ore:blockMithril>,
        "ingot": <ore:ingotMithril>,
    },
    "Lead": {
        "nugget": <ore:nuggetLead>,
        "block": <ore:blockLead>,
        "ingot": <ore:ingotLead>,
    },
    "Silver": {
        "nugget": <ore:nuggetSilver>,
        "block": <ore:blockSilver>,
        "ingot": <ore:ingotSilver>,
    },
    "Copper": {
        "nugget": <ore:nuggetCopper>,
        "block": <ore:blockCopper>,
        "ingot": <ore:ingotCopper>,
    },
    "Tin": {
        "nugget": <ore:nuggetTin>,
        "block": <ore:blockTin>,
        "ingot": <ore:ingotTin>,
    },
    "Aluminum": {
        "nugget": <ore:nuggetAluminum>,
        "block": <ore:blockAluminum>,
        "ingot": <ore:ingotAluminum>,
    },
    "Nickel": {
        "nugget": <ore:nuggetNickel>,
        "block": <ore:blockNickel>,
        "ingot": <ore:ingotNickel>,
    },
    "Uranium": {
        "nugget": <ore:nuggetUranium>,
        "block": <ore:blockUranium>,
        "ingot": <ore:ingotUranium>,
    },
    "Electrum": {
        "nugget": <ore:nuggetElectrum>,
        "block": <ore:blockElectrum>,
        "ingot": <ore:ingotElectrum>,
    },
    "Constantan": {
        "nugget": <ore:nuggetConstantan>,
        "block": <ore:blockConstantan>,
        "ingot": <ore:ingotConstantan>,
    },
    "HSLASteel": {
        "nugget": <ore:nuggetHSLASteel>,
        "block": <ore:blockHSLASteel>,
        "ingot": <ore:ingotHSLASteel>,
    },
    "Platinum": {
        "nugget": <ore:nuggetPlatinum>,
        "block": <ore:blockPlatinum>,
        "ingot": <ore:ingotPlatinum>,
    },
    "Iridium": {
        "nugget": <ore:nuggetIridium>,
        "block": <ore:blockIridium>,
        "ingot": <ore:ingotIridium>,
    },
    "Invar": {
        "nugget": <ore:nuggetInvar>,
        "block": <ore:blockInvar>,
        "ingot": <ore:ingotInvar>,
    },
    "Bronze": {
        "nugget": <ore:nuggetBronze>,
        "block": <ore:blockBronze>,
        "ingot": <ore:ingotBronze>,
    },
    "Signalum": {
        "nugget": <ore:nuggetSignalum>,
        "block": <ore:blockSignalum>,
        "ingot": <ore:ingotSignalum>,
    },
    "Lumium": {
        "nugget": <ore:nuggetLumium>,
        "block": <ore:blockLumium>,
        "ingot": <ore:ingotLumium>,
    },
    "Enderium": {
        "nugget": <ore:nuggetEnderium>,
        "block": <ore:blockEnderium>,
        "ingot": <ore:ingotEnderium>,
    },
    "Umbrium": {
        "nugget": <ore:nuggetUmbrium>,
        "block": <ore:blockUmbrium>,
        "ingot": <ore:ingotUmbrium>,
    },
    "Magnite": {
        "block": <ore:blockMagnite>,
        "ingot": <ore:ingotMagnite>,
    },
    "Abyssalnite": {
        "nugget": <ore:nuggetAbyssalnite>,
        "block": <ore:blockAbyssalnite>,
        "ingot": <ore:ingotAbyssalnite>,
    },
    "LiquifiedCoralium": {
        "nugget": <ore:nuggetLiquifiedCoralium>,
        "block": <ore:blockLiquifiedCoralium>,
        "ingot": <ore:ingotLiquifiedCoralium>,
    },
    "Dreadium": {
        "nugget": <ore:nuggetDreadium>,
        "block": <ore:blockDreadium>,
        "ingot": <ore:ingotDreadium>,
    },
    "Ethaxium": {
        "nugget": <ore:nuggetEthaxium>,
        "block": <ore:blockEthaxium>,
        "ingot": <ore:ingotEthaxium>,
    },
    "TitaniumAluminide": {
        "nugget": <ore:nuggetTitaniumAluminide>,
        "block": <ore:blockTitaniumAluminide>,
        "ingot": <ore:ingotTitaniumAluminide>,
    },
    "TitaniumIridium": {
        "nugget": <ore:nuggetTitaniumIridium>,
        "block": <ore:blockTitaniumIridium>,
        "ingot": <ore:ingotTitaniumIridium>,
    },
    "NaquadahRaw": {
        "nugget": <ore:nuggetNaquadahRaw>,
        "block": <ore:blockNaquadahRaw>,
        "ingot": <ore:ingotNaquadahRaw>,
    },
    "NaquadahRefined": {
        "nugget": <ore:nuggetNaquadahRefined>,
        "block": <ore:blockNaquadahRefined>,
        "ingot": <ore:ingotNaquadahRefined>,
    },
    "Trinium": {
        "nugget": <ore:nuggetTrinium>,
        "block": <ore:blockTrinium>,
        "ingot": <ore:ingotTrinium>,
    },
    "Tritanium": {
        "nugget": <ore:nuggetTritanium>,
        "block": <ore:blockTritanium>,
        "ingot": <ore:ingotTritanium>,
    },
    "Thorium": {
        "block": <ore:blockThorium>,
        "ingot": <ore:ingotThorium>,
    },
    "Boron": {
        "block": <ore:blockBoron>,
        "ingot": <ore:ingotBoron>,
    },
    "Lithium": {
        "block": <ore:blockLithium>,
        "ingot": <ore:ingotLithium>,
    },
    "Manganese": {
        "block": <ore:blockManganese>,
        "ingot": <ore:ingotManganese>,
    },
    "Magnesium": {
        "block": <ore:blockMagnesium>,
        "ingot": <ore:ingotMagnesium>,
    },
    "Graphite": {
        "block": <ore:blockGraphite>,
        "ingot": <ore:ingotGraphite>,
    },
    "Beryllium": {
        "block": <ore:blockBeryllium>,
        "ingot": <ore:ingotBeryllium>,
    },
    "Zirconium": {
        "block": <ore:blockZirconium>,
        "ingot": <ore:ingotZirconium>,
    },
    "Knightmetal": {
        "block": <ore:blockKnightmetal>,
        "ingot": <ore:ingotKnightmetal>,
    },
    "EbonyPsi": {
        "block": <ore:blockEbonyPsi>,
        "ingot": <ore:ingotEbonyPsi>,
    },
    "IvoryPsi": {
        "block": <ore:blockIvoryPsi>,
        "ingot": <ore:ingotIvoryPsi>,
    },
    "Psi": {
        "block": <ore:blockPsi>,
        "ingot": <ore:ingotPsi>,
    },
    "Ironwood": {
        "block": <ore:blockIronwood>,
        "ingot": <ore:ingotIronwood>,
    },
    "Fiery": {
        "block": <ore:blockFiery>,
        "ingot": <ore:ingotFiery>,
    },
    "Steelleaf": {
        "ingot": <ore:ingotSteelleaf>,
    },
    "Cobalt": {
        "nugget": <ore:nuggetCobalt>,
        "block": <ore:blockCobalt>,
        "ingot": <ore:ingotCobalt>,
    },
    "Ardite": {
        "nugget": <ore:nuggetArdite>,
        "block": <ore:blockArdite>,
        "ingot": <ore:ingotArdite>,
    },
    "Manyullyn": {
        "nugget": <ore:nuggetManyullyn>,
        "block": <ore:blockManyullyn>,
        "ingot": <ore:ingotManyullyn>,
    },
    "Knightslime": {
        "nugget": <ore:nuggetKnightslime>,
        "block": <ore:blockKnightslime>,
        "ingot": <ore:ingotKnightslime>,
    },
    "Pigiron": {
        "nugget": <ore:nuggetPigiron>,
        "block": <ore:blockPigiron>,
        "ingot": <ore:ingotPigiron>,
    },
};

static OreDictEntries as IItemStack[string][string] = {
    "Steel": {
        "nugget": <dungeontactics:steel_nugget>,
        "ingot": <dungeontactics:steel_ingot>,
        "block": <dungeontactics:steel_block>
    },
    "HSLASteel": {
        "nugget": <contenttweaker:material_part:8>,
        "ingot": <contenttweaker:material_part:7>,
        "block": <contenttweaker:sub_block_holder_0>
    },
    "Silver": {
        "nugget": <dungeontactics:silver_nugget>,
        "ingot": <dungeontactics:silver_ingot>,
        "block": <dungeontactics:silver_block>
    },
    "Mithril": {
        "nugget": <dungeontactics:mithril_nugget>,
        "ingot": <dungeontactics:mithril_ingot>,
        "block": <dungeontactics:mithril_block>
    },
    "Uranium": {
        "nugget": <immersiveengineering:metal:25>,
        "ingot": <immersiveengineering:metal:5>,
        "block": <immersiveengineering:storage:5>
    },
    "NaquadahRaw": {
        "nugget": <jsg:naquadah_raw_nugget>,
        "ingot": <jsg:naquadah_alloy_raw>,
        "block": <jsg:naquadah_block_raw>
    },
    "NaquadahRefined": {
        "nugget": <jsg:naquadah_nugget>,
        "ingot": <jsg:naquadah_alloy>,
        "block": <jsg:naquadah_block>
    },
    "Trinium": {
        "nugget": <jsg:trinium_nugget>,
        "ingot": <jsg:trinium_ingot>,
        "block": <jsg:trinium_block>
    },
    "Titanium": {
        "nugget": <jsg:titanium_nugget>,
        "ingot": <jsg:titanium_ingot>,
        "block": <jsg:titanium_block>
    },
    "Psi": {
        "ingot": <psi:material:1>,
        "block": <psi:psi_decorative:1>
    },
    "EbonyPsi": {
        "ingot": <psi:material:3>,
        "block": <psi:psi_decorative:7>
    },
    "IvoryPsi": {
        "ingot": <psi:material:4>,
        "block": <psi:psi_decorative:8>
    },
    "Magnite": {
        "block": <cavern:cave_block:3>,
        "ingot": <cavern:cave_item:1>,
    },
    "Ironwood": {
        "block": <twilightforest:block_storage>,
        "ingot": <twilightforest:ironwood_ingot>,
    },
    "Fiery": {
        "block": <twilightforest:block_storage:1>,
        "ingot": <twilightforest:fiery_ingot>,
    },
    "Steelleaf": {
        "block": <twilightforest:block_storage:2>,
        "ingot": <twilightforest:steeleaf_ingot>,
    }
};

function Init() {
    // Remove betweenlands sulfur from oredict
    val SulfurOre = <ore:oreSulfur>;
    SulfurOre.remove(<thebetweenlands:sulfur_ore>);
    
    val SulfurBlock = <ore:blockSulfur>;
    SulfurBlock.remove(<thebetweenlands:sulfur_block>);

    var blockGlitch = oreDict.get("blockGlitch");
    blockGlitch.add(<deepmoblearning:infused_ingot_block>);

    var ingotGlitch = oreDict.get("ingotGlitch");
    ingotGlitch.add(<deepmoblearning:glitch_infused_ingot>);

    Compaction.add(blockGlitch.firstItem, ingotGlitch.firstItem, 9);

    for OreDictName, OreDictMaterial in OreDictMaterials {
        logger.logInfo("Adding oredict compaction recipes for " ~ OreDictName);

        var Entries as IItemStack[string];
        if (OreDictEntries has OreDictName)
        {
            Entries = OreDictEntries[OreDictName];
        }
        else
        {
            Entries = {};

            if (OreDictMaterial has "nugget") {
                Entries["nugget"] = OreDictMaterial["nugget"].firstItem;
            }

            if (OreDictMaterial has "ingot") {
                Entries["ingot"] = OreDictMaterial["ingot"].firstItem;
            }

            if (OreDictMaterial has "block") {
                Entries["block"] = OreDictMaterial["block"].firstItem;
            }
        }

        var Block as IItemStack = Entries["block"];
        var Nugget as IItemStack = Entries["nugget"];
        var Ingot as IItemStack = Entries["ingot"];

        if (Entries has "nugget" && Entries has "ingot") {
            Compaction.add(Ingot, Nugget, 9);
            logger.logInfo("Added oredict compaction recipe from " ~ Ingot.name ~ " to " ~ Nugget.name);
        }

        if (Entries has "block" && Entries has "ingot") {
            Compaction.add(Block, Ingot, 9);
            logger.logInfo("Added oredict compaction recipe from " ~ Ingot.name ~ " to " ~ Block.name);
        }
    }
}