import crafttweaker.item.IIngredient;

import mods.zenstages.ZenStager;

import scripts.crafttweaker.staticstages.MainStage_Wilderness;
import scripts.crafttweaker.staticstages.MainStage_Settling;
import scripts.crafttweaker.staticstages.MainStage_Thaumcraft;
import scripts.crafttweaker.staticstages.MainStage_Twilight;
import scripts.crafttweaker.staticstages.MainStage_Astral;
import scripts.crafttweaker.staticstages.MainStage_Transition;
import scripts.crafttweaker.staticstages.MainStage_Engineering;
import scripts.crafttweaker.staticstages.MainStage_Expansion;
import scripts.crafttweaker.staticstages.MainStage_Automation;

import scripts.crafttweaker.staticstages.SubStage_Abyssal;

function Init() {
    // End
	MainStage_Thaumcraft.addDimension(1);

    // Nether
    MainStage_Thaumcraft.addDimension(-1);

    // Emptiness
    MainStage_Thaumcraft.addDimension(14676);

    // Betweenlands
    MainStage_Thaumcraft.addDimension(20);

    // Lost Cities
    MainStage_Transition.addDimension(111);

    // Defiled Lands
    MainStage_Twilight.addDimension(499);

    // Twilight Forest
    MainStage_Twilight.addDimension(7);

    // Caverns
    MainStage_Astral.addDimension(-50);
    MainStage_Astral.addDimension(-51);
    MainStage_Astral.addDimension(-52);
    MainStage_Astral.addDimension(-53);
    MainStage_Astral.addDimension(-54);
    MainStage_Astral.addDimension(-55);
    MainStage_Astral.addDimension(-56);
    MainStage_Astral.addDimension(-57);
    MainStage_Astral.addDimension(-58);
    MainStage_Astral.addDimension(-59);
    MainStage_Astral.addDimension(-60);

    // Abyssalcraft
	SubStage_Abyssal.addDimension(50);
	SubStage_Abyssal.addDimension(52);
	SubStage_Abyssal.addDimension(53);
	SubStage_Abyssal.addDimension(51);
}