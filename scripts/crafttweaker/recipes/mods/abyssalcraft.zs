import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import mods.zenstages.Utils;

import mods.abyssalcraft.InfusionRitual;
import mods.abyssalcraft.Rituals;
import mods.abyssalcraft.SummonRitual;

import scripts.crafttweaker.staticstages.MainStage_Thaumcraft;
import scripts.crafttweaker.staticstages.SubStage_Abyssal;

/*
    Recipe Removals
*/
static RecipesToRemove as IItemStack[] = [
	<abyssalcraft:necronomicon:0>
];

static FurnaceRecipesToRemove as IIngredient[] = [

];

/*
    Shaped Recipes
*/
static ShapedRecipes as IIngredient[][][][IItemStack] = {
	<abyssalcraft:necronomicon:0>: [
		[
			[<thaumcraft:void_seed>, <thaumcraft:curio:6>, <minecraft:dragon_breath>],
			[<thaumcraft:scribing_tools>, <thaumcraft:thaumonomicon>, <thaumcraft:primordial_pearl>],
			[<minecraft:dragon_breath>, <thaumcraft:flesh_block>, <thaumcraft:void_seed>]
		]
	]
};

static HiddenShapedRecipes as IIngredient[][][][IItemStack] = {

};


/*
    Mirrored Recipes
*/
static MirroredRecipes as IIngredient[][][][IItemStack] = {

};

/*
    Shapeless Recipes
*/
static ShapelessRecipes as IIngredient[][][IItemStack] = {

};

static HiddenShapelessRecipes as IIngredient[][][IItemStack] = {

};

function Init() {
    // Adjust all the recipes
    Util.RemoveRecipes(RecipesToRemove);
	Util.RemoveFurnace(FurnaceRecipesToRemove);
	Util.AddShaped(ShapedRecipes, false, false);
    Util.AddShaped(HiddenShapedRecipes, false, true);
    Util.AddShaped(MirroredRecipes, true, false);
    Util.AddShapeless(ShapelessRecipes, false);
    Util.AddShapeless(HiddenShapelessRecipes, true);

    // Add new rituals
}
