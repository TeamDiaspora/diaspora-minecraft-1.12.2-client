import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import mods.zenstages.Utils;

import mods.tconstruct.Casting as Casting;

/*
    Recipe Removals
*/
static RecipesToRemove as IItemStack[] = [
    <dungeontactics:wooden_knife>,
    <dungeontactics:bone_knife>,
    <dungeontactics:stone_knife>,
    <dungeontactics:iron_knife>,
    <dungeontactics:silver_knife>,
    <dungeontactics:golden_knife>,
    <dungeontactics:steel_knife>,
    <dungeontactics:diamond_knife>,
    <dungeontactics:mithril_knife>,
    <dungeontactics:wooden_cestus>,
    <dungeontactics:bone_cestus>,
    <dungeontactics:stone_cestus>,
    <dungeontactics:iron_cestus>,
    <dungeontactics:silver_cestus>,
    <dungeontactics:golden_cestus>,
    <dungeontactics:steel_cestus>,
    <dungeontactics:diamond_cestus>,
    <dungeontactics:mithril_cestus>,
    <dungeontactics:slingshot>
];

static RecipesToRemove2 as string[] = [
    "dungeontactics:misc/convenience/sticks_from_logs",
];

static FurnaceRecipesToRemove as IIngredient[] = [

];

/*
    Shaped Recipes
*/
static ShapedRecipes as IIngredient[][][][IItemStack] = {
    <dungeontactics:slingshot>:
    [
        [
            [<ore:stickWood>, <ore:string>, <ore:stickWood>],
            [null, <ore:stickWood>, null],
            [null, <ore:stickWood>, null],
        ]
    ]
};

static HiddenShapedRecipes as IIngredient[][][][IItemStack] = {

};


/*
    Mirrored Recipes
*/
static MirroredRecipes as IIngredient[][][][IItemStack] = {

};

/*
    Shapeless Recipes
*/
static ShapelessRecipes as IIngredient[][][IItemStack] = {

};

static HiddenShapelessRecipes as IIngredient[][][IItemStack] = {

};

function Init() {
    Util.RemoveRecipes(RecipesToRemove);
	Util.RemoveFurnace(FurnaceRecipesToRemove);
	Util.AddShaped(ShapedRecipes, false, false);
    Util.AddShaped(HiddenShapedRecipes, false, true);
    Util.AddShaped(MirroredRecipes, true, false);
    Util.AddShapeless(ShapelessRecipes, false);
    Util.AddShapeless(HiddenShapelessRecipes, true);

    // Add tinkers recipe(s)
    Casting.addTableRecipe(<dungeontactics:mithril_ingot>, <ore:ingotThaumium>, <liquid:steel>, 144, true, 15);
}
