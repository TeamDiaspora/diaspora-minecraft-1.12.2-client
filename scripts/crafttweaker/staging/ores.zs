import crafttweaker.item.IIngredient;
import crafttweaker.item.IItemStack;

import mods.zenstages.Stage;
import mods.zenstages.ZenStager;
import mods.orestages.OreStages;

import scripts.crafttweaker.staticstages.MainStage_Wilderness;
import scripts.crafttweaker.staticstages.MainStage_Settling;
import scripts.crafttweaker.staticstages.MainStage_Thaumcraft;
import scripts.crafttweaker.staticstages.MainStage_Twilight;
import scripts.crafttweaker.staticstages.MainStage_Astral;
import scripts.crafttweaker.staticstages.MainStage_Transition;
import scripts.crafttweaker.staticstages.MainStage_Engineering;
import scripts.crafttweaker.staticstages.MainStage_Expansion;
import scripts.crafttweaker.staticstages.MainStage_Automation;
import scripts.crafttweaker.staticstages.SubStage_Abyssal;
import scripts.crafttweaker.staticstages.Stage_Disabled;

static ReplacementItemsForStage as IIngredient[][][string] = {
	Stage_Disabled.stage: [
        [<cavern:cave_block:4>],
	],

	MainStage_Thaumcraft.stage: [
		[<ore:oreEmerald>],
		[<ore:oreLead>],
        [<ore:oreSilver>],
        [<ore:oreAmber>],
        [<ore:oreCinnabar>],
        [<ore:oreMithril>],
        [<ebwizardry:crystal_ore>]
	],

    SubStage_Abyssal.stage: [
		[<ore:oreHephaestite>],
		[<ore:oreAbyssalnite>],
		[<ore:oreCoralium>],
		[<ore:oreCoraliumStone>],
		[<ore:oreSaltpeter>],
		// [<ore:oreSulfur>],
	],

    MainStage_Astral.stage: [
		[<ore:oreAstralStarmetal>],
		[<ore:oreAquamarine>],
		[<astralsorcery:blockcustomore:0>],
		[<cavern:cave_block:7>],
		[<cavern:cave_block:5>],
		[<cavern:cave_block:2>],
		[<cavern:cave_block:0>],
        [<ore:oreCopper>],
        [<ore:oreAluminum>],
        [<ore:oreTin>],
        [<ore:oreNickel>],
        [<ore:oreUranium>],
        [<ore:oreTitanium>]
	],

    MainStage_Transition.stage: [
		[<ore:oreTritanium>],
		[<ore:oreDilithium>],
	],

    MainStage_Engineering.stage: [

        [<ore:oreClathrateOilShale>],
        [<ore:oreClathrateOilSand>]
	],

    MainStage_Expansion.stage: [
		[<ore:oreBoron>],
		[<ore:oreLithium>],
		[<ore:oreThorium>],
		[<ore:oreMagnesium>],
		[<ore:orePlatinum>],
        [<ore:oreDimensionalShard>],
        [<ore:oreClathrateRedstone>],
        [<ore:oreClathrateEnder>, <minecraft:end_stone:0>],
        [<ore:oreClathrateGlowstone>, <minecraft:netherrack:0>],
	],

    MainStage_Automation.stage: [
		[<ore:oreCertusQuartz>],
	],
};

function Init() {
	for StageName, Replacements in ReplacementItemsForStage {
		var Stage as Stage = ZenStager.getStage(StageName);

		for Replacement in Replacements {
            if (Replacement.length == 1)
            {
                Stage.addOreReplacement(Replacement[0], false);
            }
            else
            {
			    Stage.addOreReplacement(Replacement[0], Replacement[1].items[0], false);
            }
		}
	}
}