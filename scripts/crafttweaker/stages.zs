#priority 2001

import mods.zenstages.ZenStager;
import mods.zenstages.Stage;

// Core Stages
var MainStage_Wilderness as Stage = ZenStager.initStage("mainstage_wilderness");
var MainStage_Settling as Stage = ZenStager.initStage("mainstage_settling");
var MainStage_Thaumcraft as Stage = ZenStager.initStage("mainstage_thaumcraft");
var MainStage_Twilight as Stage = ZenStager.initStage("mainstage_twilight");
var MainStage_Astral as Stage = ZenStager.initStage("mainstage_astral");
var MainStage_Transition as Stage = ZenStager.initStage("mainstage_transition");
var MainStage_Engineering as Stage = ZenStager.initStage("mainstage_engineering");
var MainStage_Expansion as Stage = ZenStager.initStage("mainstage_expansion");
var MainStage_Automation as Stage = ZenStager.initStage("mainstage_automation");

// Sub Stages
var SubStage_Abyssal as Stage = ZenStager.initStage("substage_abyssal");

// Mob Stages
var MobStage_Awakening as Stage = ZenStager.initStage("mobstage_awakening");

// Special Stages
var Stage_Disabled as Stage = ZenStager.initStage("stage_disabled");