import mods.bonsaitrees.SoilCompatibility as SoilCompatibility;
import mods.bonsaitrees.SoilStats as SoilStats;
import mods.bonsaitrees.TreeDrops as TreeDrops;
import mods.bonsaitrees.TreeGrowth as TreeGrowth;

function Init() {
    // Modify existing trees
    SoilCompatibility.addCompatibleTagToTree("minecraft:reeds", "snad");
}