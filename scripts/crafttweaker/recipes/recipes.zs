#priority -10


function Init() {
    scripts.crafttweaker.recipes.mods.minecraft.Init();
    scripts.crafttweaker.recipes.mods.abyssalcraft.Init();
    scripts.crafttweaker.recipes.mods.backpacks.Init();
    scripts.crafttweaker.recipes.mods.betweenlands.Init();
    scripts.crafttweaker.recipes.mods.bloodmagic.Init();
    scripts.crafttweaker.recipes.mods.dungeontactics.Init();
    scripts.crafttweaker.recipes.mods.enderstorage.Init();
    scripts.crafttweaker.recipes.mods.immersiveengineering.Init();
    scripts.crafttweaker.recipes.mods.jsg.Init();
    scripts.crafttweaker.recipes.mods.lycanites.Init();
    scripts.crafttweaker.recipes.mods.openblocks.Init();
    scripts.crafttweaker.recipes.mods.prefab.Init();
    scripts.crafttweaker.recipes.mods.simplestorage.Init();
    scripts.crafttweaker.recipes.mods.thaumcraft.Init();
}

function InitOverrides() {

}