#priority 2000

import mods.zenstages.ZenStager;
import mods.zenstages.Stage;

// Core Stages
static MainStage_Wilderness as Stage = ZenStager.getStage("mainstage_wilderness");
static MainStage_Settling as Stage = ZenStager.getStage("mainstage_settling");
static MainStage_Thaumcraft as Stage = ZenStager.getStage("mainstage_thaumcraft");
static MainStage_Twilight as Stage = ZenStager.getStage("mainstage_twilight");
static MainStage_Astral as Stage = ZenStager.getStage("mainstage_astral");
static MainStage_Transition as Stage = ZenStager.getStage("mainstage_transition");
static MainStage_Engineering as Stage = ZenStager.getStage("mainstage_engineering");
static MainStage_Expansion as Stage = ZenStager.getStage("mainstage_expansion");
static MainStage_Automation as Stage = ZenStager.getStage("mainstage_automation");

// Sub Stages
static SubStage_Abyssal as Stage = ZenStager.getStage("substage_abyssal");

// Mob Stages
static MobStage_Awakening as Stage = ZenStager.getStage("mobstage_awakening");

// Special Stages
static Stage_Disabled as Stage = ZenStager.getStage("stage_disabled");