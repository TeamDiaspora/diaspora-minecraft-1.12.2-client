#priority -10

function Init() {
    scripts.crafttweaker.staging.oredict.Init();
    scripts.crafttweaker.staging.ores.Init();
    scripts.crafttweaker.staging.modid.Init();
    scripts.crafttweaker.staging.mobs.Init();
    scripts.crafttweaker.staging.dimensions.Init();

    scripts.crafttweaker.staging.mods.abyssalcraft.Init();
    scripts.crafttweaker.staging.mods.appliedenergistics.Init();
    scripts.crafttweaker.staging.mods.astralsorcery.Init();
    scripts.crafttweaker.staging.mods.betweenlands.Init();
    scripts.crafttweaker.staging.mods.cavern.Init();
    scripts.crafttweaker.staging.mods.immersiveengineering.Init();
    scripts.crafttweaker.staging.mods.openblocks.Init();
    scripts.crafttweaker.staging.mods.thermal.Init();
}

function InitOverrides() {
    scripts.crafttweaker.staging.oredict.InitOverrides();
    scripts.crafttweaker.staging.mods.abyssalcraft.InitOverrides();
    scripts.crafttweaker.staging.mods.appliedenergistics.InitOverrides();
    scripts.crafttweaker.staging.mods.astralsorcery.InitOverrides();
    scripts.crafttweaker.staging.mods.betweenlands.InitOverrides();
    scripts.crafttweaker.staging.mods.cavern.InitOverrides();
    scripts.crafttweaker.staging.mods.immersiveengineering.InitOverrides();
    scripts.crafttweaker.staging.mods.openblocks.InitOverrides();
    scripts.crafttweaker.staging.mods.thermal.InitOverrides();
}