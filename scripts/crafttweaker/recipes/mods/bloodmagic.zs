import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import mods.zenstages.Utils;

/*
    Recipe Removals
*/
static RecipesToRemove as IItemStack[] = [
    <guideapi:bloodmagic-guide>,
    <lycanitesmobs:blood_chili>
];

static FurnaceRecipesToRemove as IIngredient[] = [

];

/*
    Shaped Recipes
*/
static ShapedRecipes as IIngredient[][][][IItemStack] = {
    <guideapi:bloodmagic-guide>:
    [
        [
            [<tombstone:familiar_receptacle>, <dungeontactics:soulgem>, <tombstone:familiar_receptacle>],
            [<lycanitesmobs:blood_chili>, <tombstone:book_of_disenchantment>, <lycanitesmobs:blood_chili>],
            [<tombstone:scroll_of_knowledge>, <lycanitesmobs:blood_chili>, <tombstone:scroll_of_knowledge>]
        ]
    ],
    <lycanitesmobs:blood_chili>: [
        [
            [<tconstruct:edible:33>, <lycanitesmobs:cooked_chupacabra_meat>, <tconstruct:edible:33>],
            [<tconstruct:slime_channel:3>, <minecraft:bowl>, <tconstruct:slime_channel:3>],
            [<minecraft:bowl>, <minecraft:bowl>, <minecraft:bowl>]
        ]
    ]
};

static HiddenShapedRecipes as IIngredient[][][][IItemStack] = {
    <twilightforest:fiery_blood> * 16:
    [
        [
            [<bloodmagic:demon_crystal:3>, <bloodmagic:sigil_lava>, <bloodmagic:demon_crystal:3>],
            [<thebetweenlands:dentrothyst_vial>, <forge:bucketfilled>.onlyWithTag({FluidName: "lifeessence", Amount: 1000}), <thebetweenlands:dentrothyst_vial:2>],
            [<bloodmagic:demon_crystal:3>, <bloodmagic:sigil_transposition>, <bloodmagic:demon_crystal:3>]
        ]
    ]
};


/*
    Mirrored Recipes
*/
static MirroredRecipes as IIngredient[][][][IItemStack] = {

};

/*
    Shapeless Recipes
*/
static ShapelessRecipes as IIngredient[][][IItemStack] = {

};

static HiddenShapelessRecipes as IIngredient[][][IItemStack] = {

};

function Init() {
    Util.RemoveRecipes(RecipesToRemove);
	Util.RemoveFurnace(FurnaceRecipesToRemove);
	Util.AddShaped(ShapedRecipes, false, false);
    Util.AddShaped(HiddenShapedRecipes, false, true);
    Util.AddShaped(MirroredRecipes, true, false);
    Util.AddShapeless(ShapelessRecipes, false);
    Util.AddShapeless(HiddenShapelessRecipes, true);
}
