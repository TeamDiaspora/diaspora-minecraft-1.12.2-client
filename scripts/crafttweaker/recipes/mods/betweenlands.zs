import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import mods.zenstages.Utils;

/*
    Recipe Removals
*/
static RecipesToRemove as IItemStack[] = [

];

static FurnaceRecipesToRemove as IIngredient[] = [

];

/*
    Shaped Recipes
*/
static ShapedRecipes as IIngredient[][][][IItemStack] = {
    <thebetweenlands:swamp_talisman>:
    [
        [
            [<minecraft:ender_pearl>, <thebetweenlands:swamp_talisman:4>, <ore:dustGrave>],
            [<thebetweenlands:swamp_talisman:1>, <tombstone:tablet_of_home>, <thebetweenlands:swamp_talisman:3>],
            [<tombstone:dust_of_vanishing>, <thebetweenlands:swamp_talisman:2>, <minecraft:ender_pearl>]
        ]
    ],

    <thebetweenlands:swamp_talisman:1>:
    [
        [
            [null, <thaumcraft:nitor_green>, <ore:treeSapling>],
            [<thaumcraft:nitor_green>, <minecraft:ender_pearl>, <thaumcraft:nitor_green>],
            [<tconstruct:tool_rod>.withTag({Material: "slime"}), <thaumcraft:nitor_green>, null]
        ]
    ],

    <thebetweenlands:swamp_talisman:2>:
    [
        [
            [null, null, <tconstruct:edible:3>],
            [null, <tconstruct:materials:17>, null],
            [<tconstruct:materials:17>, null, null]
        ]
    ],

    <thebetweenlands:swamp_talisman:3>:
    [
        [
            [null, <ore:ingotSteel>, <ore:ingotSteel>],
            [<minecraft:slime_ball>, <tconstruct:pick_head>.withTag({Material: "terra_thaumium"}), <ore:ingotSteel>],
            [<tconstruct:tool_rod>.withTag({Material: "cactus"}), <minecraft:slime_ball>, null]
        ]
    ],

    <thebetweenlands:swamp_talisman:4>:
    [
        [
            [<thaumcraft:crystal_essence>.withTag({Aspects: [{amount: 1, key: "permutatio"}]}), <thaumcraft:crystal_essence>.withTag({Aspects: [{amount: 1, key: "terra"}]}), null],
            [<thaumcraft:crystal_essence>.withTag({Aspects: [{amount: 1, key: "perditio"}]}), <thaumcraft:salis_mundus>, <thaumcraft:crystal_essence>.withTag({Aspects: [{amount: 1, key: "herba"}]})],
            [<ore:ingotSteel>, <thaumcraft:crystal_essence>.withTag({Aspects: [{amount: 1, key: "exanimis"}]}), <thaumcraft:crystal_essence>.withTag({Aspects: [{amount: 1, key: "fabrico"}]})]]
    ],
};

static HiddenShapedRecipes as IIngredient[][][][IItemStack] = {

};

/*
    Mirrored Recipes
*/
static MirroredRecipes as IIngredient[][][][IItemStack] = {

};

/*
    Shapeless Recipes
*/
static ShapelessRecipes as IIngredient[][][IItemStack] = {

};

static HiddenShapelessRecipes as IIngredient[][][IItemStack] = {

};

function Init() {
    Util.RemoveRecipes(RecipesToRemove);
	Util.RemoveFurnace(FurnaceRecipesToRemove);
	Util.AddShaped(ShapedRecipes, false, false);
    Util.AddShaped(HiddenShapedRecipes, false, true);
    Util.AddShaped(MirroredRecipes, true, false);
    Util.AddShapeless(ShapelessRecipes, false);
    Util.AddShapeless(HiddenShapelessRecipes, true);
}
