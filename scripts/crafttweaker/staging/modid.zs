#priority 1900

import mods.zenstages.ZenStager;

import scripts.crafttweaker.staticstages.MainStage_Wilderness;
import scripts.crafttweaker.staticstages.MainStage_Settling;
import scripts.crafttweaker.staticstages.MainStage_Thaumcraft;
import scripts.crafttweaker.staticstages.MainStage_Twilight;
import scripts.crafttweaker.staticstages.MainStage_Astral;
import scripts.crafttweaker.staticstages.MainStage_Transition;
import scripts.crafttweaker.staticstages.MainStage_Engineering;
import scripts.crafttweaker.staticstages.MainStage_Expansion;
import scripts.crafttweaker.staticstages.MainStage_Automation;

import scripts.crafttweaker.staticstages.SubStage_Abyssal;

static StagedMods as string[][string] = {
    MainStage_Wilderness.stage : [
        // Combat
        "dungeontactics", // Dungeon Tactics
        "spartanweaponry", // Spartan Weaponry

        // General Mods
        "baubles", // Baubles
        "notreepunching", // No Tree Punching
        "wolfarmor", // Wolf Armor and Storage
        "antiqueatlas", // Antique Atlas
        "akashictome",
        "doggytalents", // Doggy Talents
        "inventorypets", // Inventory Pets

        // Building
        "ropebridge", // Rope Bridge

        // Housing
        "yurtmod", // Nomadic Tents
	],

    MainStage_Settling.stage : [
        // Farming/Food
        "agricraft", // Agricraft
        "skewers", // Skewers
        "culinaryconstruct", // Culinary Construct
        "advanced-fishing", // Advanced Fishing
        "harvestcraft", // Pam's Harvestcraft

        // Storage
        "storagenetwork", // Simple Storage Network
        "storagedrawers", // Storage Drawers

        // General Mods
        "waystones", // Waystones (Craftable)
        "openblocks", // OpenBlocks
        "uppers", // Uppers
        "potioncore", // Potion Core

        // Housing
        "prefab", // Prefab

        // Building
        "blockcraftery", // Blockcraftery
        "chisel", // Chisel
	],

    MainStage_Thaumcraft.stage : [
        // General Magic
        //"thaumcraft", // Thaumcraft
        //"thaumictinkerer", // Thaumic Tinkerer
        "spartanweaponryarcana",
        "ebwizardry", // Electroblob's Wizardy

        // General Mods
        "tconstruct", // Tinkers Construct
        "vc", // Viescraft

        // Farming/Food
        "bonsaitrees", // Bonsai Trees

        // Building
        "betterbuilderswands", // Better Builder's Wands
	],

    SubStage_Abyssal.stage : [
        "abyssalcraft", // AbyssalCraft
        "acintegration", // AbyssalCraft Integration
    ],

    MainStage_Twilight.stage : [
        // Combat
        "lycanitesmobs", // Lycanites Mobs

        // Storage
        "enderstorage", // Ender Storage

        // General Magic
        "defiledlands", // Defiled Lands
        "bloodmagic", // Blood Magic

        // Dimensions
        "twilightforest", // Twilight Forest
	],

    MainStage_Astral.stage : [
        // Magic -> Tech Transition
        "jsg", // Stargate

        // General Magic
        "astralsorcery", // Astral Sorcery

        // General Tech
        "immersiveengineering", // immersive engineering
        
        // Dimensions
        "cavern", // Caverns II
	],

    MainStage_Transition.stage : [
        // Magic -> Tech Transition
        "psi", // Psi
        "rpsideas", // Random PSIdeas

        // Combat
        "matteroverdrive", // Matter Overdrive

        // Dimensions
        //"lost_cities", // Lost Cities
	],

    MainStage_Engineering.stage : [
        // General Tech
        "immersivetech", // Immersive Tech
	],

    MainStage_Expansion.stage : [
        // General Tech
        "rftools", // RFTools
        "thermalexpansion", // Thermal Expansion
        "thermaldynamics",
        "thermalinnovation",
        "nuclearcraft", // NuclearCraft
	],

    MainStage_Automation.stage : [
        // Combat
        "deepmoblearning", // Deep Mob Learning
        "powersuits", // modular powersuits

        // Storage
        "appliedenergistics2", // Applied Energistics
        "extracells", // Extra Cells 2

        // General Tech
        "advancedrocketry", // Advanced Rocketry
        "opencomputers", // Open Computers
        "openscreens", // Open Screens
	],
};

function Init() {
	for StageName, ModId in StagedMods {
		ZenStager.getStage(StageName).addModId(ModId, true);
	}
}

function ContainsMod(ModId as string) as string {
	for Stage, Mods in StagedMods {
		if (Mods has ModId) {
			return Stage;
		}
	}
    
	return "";
}