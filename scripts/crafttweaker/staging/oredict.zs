import crafttweaker.item.IIngredient;

import mods.zenstages.ZenStager;

import scripts.crafttweaker.staticstages.MainStage_Wilderness;
import scripts.crafttweaker.staticstages.MainStage_Settling;
import scripts.crafttweaker.staticstages.MainStage_Thaumcraft;
import scripts.crafttweaker.staticstages.MainStage_Twilight;
import scripts.crafttweaker.staticstages.MainStage_Astral;
import scripts.crafttweaker.staticstages.MainStage_Transition;
import scripts.crafttweaker.staticstages.MainStage_Engineering;
import scripts.crafttweaker.staticstages.MainStage_Expansion;
import scripts.crafttweaker.staticstages.MainStage_Automation;

import scripts.crafttweaker.staticstages.SubStage_Abyssal;

static StagedItems as IIngredient[][string] = {
	MainStage_Settling.stage: [
        <ore:dustIron>,
        <ore:dustGold>,
		<ore:gearWood>,
		<ore:gearStone>,
		<ore:gearIron>,
		<ore:gearGold>,
		
	],
    MainStage_Thaumcraft.stage: [
        <ore:oreMithril>,
        <ore:ingotMithril>,
        <ore:dustMithril>,
        <ore:oreLead>,
        <ore:ingotLead>,
        <ore:dustLead>,
        <ore:plateIron>,
        <ore:plateLead>,
        <ore:plateThaumium>,
        <ore:dustSteel>,
        <ore:ingotSilver>,
		<ore:ingotSteel>,
        <ore:gearSteel>,
        <ore:plateSilver>,
        <ore:plateSteel>,
        <ore:oreSilver>,
        <ore:dustSilver>,
        <ore:blockSilver>,
        <ore:blockMithril>,
        <ore:blockSteel>,
        <ore:nuggetSteel>,
        <ore:nuggetSilver>,
        <ore:nuggetMithril>,
    ],
    MainStage_Astral.stage: [
        <ore:nuggetCopper>,
        <ore:blockCopper>,
        <ore:plateCopper>,
        <ore:ingotCopper>,
        <ore:gearCopper>,
        <ore:dustCopper>,
        <ore:oreCopper>,
        <ore:nuggetTin>,
        <ore:blockTin>,
        <ore:plateTin>,
        <ore:ingotTin>,
        <ore:gearTin>,
        <ore:dustTin>,
        <ore:oreTin>,
        <ore:nuggetAluminum>,
        <ore:blockAluminum>,
        <ore:plateAluminum>,
        <ore:ingotAluminum>,
        <ore:gearAluminum>,
        <ore:dustAluminum>,
        <ore:oreAluminum>,
        <ore:nuggetNickel>,
        <ore:blockNickel>,
        <ore:plateNickel>,
        <ore:ingotNickel>,
        <ore:gearNickel>,
        <ore:dustNickel>,
        <ore:oreNickel>,
        <ore:nuggetUranium>,
        <ore:blockUranium>,
        <ore:plateUranium>,
        <ore:ingotUranium>,
        <ore:dustUranium>,
        <ore:oreUranium>,
        <ore:nuggetElectrum>,
        <ore:blockElectrum>,
        <ore:plateElectrum>,
        <ore:ingotElectrum>,
        <ore:gearElectrum>,
        <ore:dustElectrum>,
        <ore:nuggetConstantan>,
        <ore:blockConstantan>,
        <ore:plateConstantan>,
        <ore:ingotConstantan>,
        <ore:gearConstantan>,
        <ore:dustConstantan>,
        <ore:ingotHSLASteel>,
        <ore:plateHSLASteel>,
        <ore:gearHSLASteel>,
        <ore:dustHSLASteel>,
        <ore:nuggetHSLASteel>,
        <ore:blockHSLASteel>,
    ],
    MainStage_Engineering: [
        <ore:clathrateOil>,
        <ore:oreClathrateOilSand>,
        <ore:oreClathrateOilShale>,
    ],
    MainStage_Expansion: [
        <ore:blockGlassHardened>,
        <ore:blockRockwool>,
        <ore:clathrateEnder>,
        <ore:clathrateGlowstone>,
        <ore:clathrateRedstone>,
        <ore:oreClathrateEnder>,
        <ore:oreClathrateGlowstone>,
        <ore:oreClathrateRedstone>,
        <ore:itemBiomass>,
        <ore:itemBiomassRich>,
        <ore:itemBioblend>,
        <ore:itemBioblendRich>,
        <ore:dustPyrotheum>,
        <ore:dustCryotheum>,
        <ore:dustAerotheum>,
        <ore:dustPetrotheum>,
        <ore:dustMana>,
        <ore:dustBlitz>,
        <ore:dustBlizz>,
        <ore:dustBasalz>,
        <ore:nuggetPlatinum>,
        <ore:blockPlatinum>,
        <ore:platePlatinum>,
        <ore:ingotPlatinum>,
        <ore:gearPlatinum>,
        <ore:dustPlatinum>,
        <ore:orePlatinum>,
        <ore:nuggetIridium>,
        <ore:blockIridium>,
        <ore:plateIridium>,
        <ore:ingotIridium>,
        <ore:gearIridium>,
        <ore:dustIridium>,
        <ore:oreIridium>,
        <ore:nuggetInvar>,
        <ore:blockInvar>,
        <ore:plateInvar>,
        <ore:ingotInvar>,
        <ore:gearInvar>,
        <ore:dustInvar>,
        <ore:nuggetBronze>,
        <ore:blockBronze>,
        <ore:plateBronze>,
        <ore:ingotBronze>,
        <ore:gearBronze>,
        <ore:dustBronze>,
        <ore:nuggetSignalum>,
        <ore:blockSignalum>,
        <ore:plateSignalum>,
        <ore:ingotSignalum>,
        <ore:gearSignalum>,
        <ore:dustSignalum>,
        <ore:nuggetLumium>,
        <ore:blockLumium>,
        <ore:plateLumium>,
        <ore:ingotLumium>,
        <ore:gearLumium>,
        <ore:dustLumium>,
        <ore:nuggetEnderium>,
        <ore:blockEnderium>,
        <ore:plateEnderium>,
        <ore:ingotEnderium>,
        <ore:gearEnderium>,
        <ore:dustEnderium>,
    ]
};

function Init() {
	for Stage, Items in StagedItems {
        if (Items.length > 0)
        {
		    ZenStager.getStage(Stage).addIngredients(Items);
        }
	}
}

function InitOverrides() {
    for StageName, Ingredients in StagedItems {
        for Ingredient in Ingredients {
            var bOverrideNeeded as bool = false;
            var ModIds as string[] = [];
            for Item in Ingredient.items {
                var ModId as string = Item.definition.owner;
                var ModStage as string = scripts.crafttweaker.staging.modid.ContainsMod(ModId);
                var bModWasStaged as bool = ModStage != "";

                if (bModWasStaged && StageName != ModStage) {
                    bOverrideNeeded = true;
                    ModIds += ModId;
                    break;
                }
            }

            if (bOverrideNeeded)
            {
                for ModId in ModIds {
                    if (Ingredient.items.length > 0)
                    {
                        ZenStager.addModItemOverrides(ModId, Ingredient);
                    }
                }
            }
        }
	}
}