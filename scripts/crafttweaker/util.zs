#priority 2100

import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;

import mods.zenstages.ZenStager;

zenClass UtilClass {
	zenConstructor() {
	}

    var hiddenId as int = 0;
	function AddShapeless(map as IIngredient[][][IItemStack], isHidden as bool) {
		for item, itemRecipes in map {
			for recipe in itemRecipes {
                if (isHidden) {
                    var recipeName = "ct_hidden_" ~ hiddenId ~ "_" ~ item.name;
                    hiddenId = hiddenId + 1;
                    recipes.addHiddenShapeless(recipeName, item, recipe);
                } else {
				    recipes.addShapeless(item, recipe);
                }
			}
		}
	}


	function AddShaped(map as IIngredient[][][][IItemStack], isMirrored as bool, isHidden as bool) {
        if (isHidden && isMirrored)
        {
            logger.logError("attempt to add recipe that is hidden and mirrored, recipe will not be hidden!");
        }
        
		for item, itemRecipes in map {
			for recipe in itemRecipes {
				if (isMirrored) {
                    recipes.addShapedMirrored(item, recipe);
				} else {
                    if (isHidden) {
                        var recipeName = "ct_hidden_" ~ hiddenId ~ "_" ~ item.name;
                        hiddenId = hiddenId + 1;
                        recipes.addHiddenShaped(recipeName, item, recipe);
                    } else {
					    recipes.addShaped(item, recipe);
                    }
				}
			}
		}
	}

	function RemoveRecipes(removals as IItemStack[]) {
		for toRemove in removals {
			recipes.remove(toRemove);
		}
	}

	function RemoveRecipes(removals as string[]) {
		for toRemove in removals {
			recipes.removeByRegex(toRemove);
		}
	}

	function AddFurnace(recipesToAdd as IIngredient[][IItemStack]) {
		for output, inputs in recipesToAdd {
			for input in inputs {
				furnace.addRecipe(output, input);
			}
		}
	}

	function RemoveFurnace(removals as IIngredient[]) {
		for toRemove in removals {
			furnace.remove(toRemove);
		}
	}

	function RemoveFurnace(removals as IIngredient[IIngredient]) {
		for input, output in removals {
			furnace.remove(input, output);
		}
	}
}

global Util as UtilClass = UtilClass();