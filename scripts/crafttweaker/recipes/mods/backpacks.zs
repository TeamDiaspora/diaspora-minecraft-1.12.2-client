import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import mods.zenstages.Utils;

/*
    Recipe Removals
*/
static RecipesToRemove as IItemStack[] = [
    <quark:backpack>,
    <wearablebackpacks:backpack>,
    <simplybackpacks:commonbackpack>,
    <simplybackpacks:uncommonbackpack>,
    <simplybackpacks:rarebackpack>,
    <simplybackpacks:epicbackpack>,
];

static FurnaceRecipesToRemove as IIngredient[] = [

];

/*
    Shaped Recipes
*/
static ShapedRecipes as IIngredient[][][][IItemStack] = {
    <quark:backpack>: [
        [
            [<ore:string>, null, <ore:string>],
            [<ore:blockWool>, null, <ore:blockWool>],
            [null, <ore:blockWool>, null]
        ]
    ],

    <wearablebackpacks:backpack>: [
        [
            [<ore:leather>, <dungeontactics:magic_powder>, <ore:leather>],
            [<ore:leather>, <quark:backpack>, <ore:leather>],
            [null, <ore:leather>, null]
        ]
    ],

    <simplybackpacks:commonbackpack>: [
        [
            [<thaumcraft:fabric>, <dungeontactics:magic_powder>, <thaumcraft:fabric>],
            [<thaumcraft:fabric>, <wearablebackpacks:backpack>.anyDamage(), <thaumcraft:fabric>],
            [null, <thaumcraft:fabric>, null]
        ]
    ],

    <simplybackpacks:uncommonbackpack>: [
        [
            [<ore:ingotBrass>, <thaumcraft:nitor_yellow>, <ore:ingotBrass>],
            [<dungeontactics:magic_powder>, <simplybackpacks:commonbackpack>.anyDamage(), <dungeontactics:magic_powder>],
            [<thaumcraft:fabric>, <dungeontactics:magic_powder>, <thaumcraft:fabric>]
        ]
    ],

    <simplybackpacks:rarebackpack>: [
        [
            [<ore:ingotSyrmorite>, <thaumcraft:vis_resonator>, <ore:ingotSyrmorite>],
            [<ore:slimecrystal>, <simplybackpacks:uncommonbackpack>.anyDamage(), <ore:slimecrystal>],
            [<thaumcraft:ingot>, <ore:ingotKnightslime>, <thaumcraft:ingot>]
        ]
    ],

    <simplybackpacks:epicbackpack>: [
        [
            [<minecraft:end_crystal>, <minecraft:nether_star>, <minecraft:end_crystal>],
            [<thaumcraft:phial:1>.withTag({Aspects: [{amount: 10, key: "vacuos"}]}).onlyWithTag({Aspects: [{amount: 10, key: "vacuos"}]}), <simplybackpacks:rarebackpack>.anyDamage(), <thaumcraft:phial:1>.withTag({Aspects: [{amount: 10, key: "vacuos"}]}).onlyWithTag({Aspects: [{amount: 10, key: "vacuos"}]})],
            [<tconstruct:metal:3>, <quark:ender_watcher>, <tconstruct:metal:3>]
        ]
    ],
};

static HiddenShapedRecipes as IIngredient[][][][IItemStack] = {

};


/*
    Mirrored Recipes
*/
static MirroredRecipes as IIngredient[][][][IItemStack] = {

};

/*
    Shapeless Recipes
*/
static ShapelessRecipes as IIngredient[][][IItemStack] = {

};

static HiddenShapelessRecipes as IIngredient[][][IItemStack] = {

};

function Init() {
    Util.RemoveRecipes(RecipesToRemove);
	Util.RemoveFurnace(FurnaceRecipesToRemove);
	Util.AddShaped(ShapedRecipes, false, false);
    Util.AddShaped(HiddenShapedRecipes, false, true);
    Util.AddShaped(MirroredRecipes, true, false);
    Util.AddShapeless(ShapelessRecipes, false);
    Util.AddShapeless(HiddenShapelessRecipes, true);
}
