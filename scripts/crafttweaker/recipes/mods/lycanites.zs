import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import mods.zenstages.Utils;

/*
    Recipe Removals
*/
static RecipesToRemove as IItemStack[] = [
    <lycanitesmobs:soulgazer>,
    <lycanitesmobs:summoningstaff>
];

static FurnaceRecipesToRemove as IIngredient[] = [

];

/*
    Shaped Recipes
*/
static ShapedRecipes as IIngredient[][][][IItemStack] = {
    <lycanitesmobs:soulgazer>:
    [
        [
            [<twilightforest:fiery_blood>, <minecraft:dragon_breath>, <twilightforest:fiery_blood>],
            [<abyssalcraft:shadowgem>, <abyssalcraft:essence>, <abyssalcraft:shadowgem>],
            [<twilightforest:fiery_blood>, <minecraft:dragon_breath>, <twilightforest:fiery_blood>]
        ]
    ],

    <lycanitesmobs:summoningstaff>:
    [
        [
            [<thaumicrestoration:item_wand_cap_tr>, <lycanitesmobs:soulgazer>, <thaumicrestoration:item_wand_cap_tr>],
            [<ore:plateGold>, <thaumicwands:item_wand_rod:7>, <ore:plateGold>],
            [<ore:plateGold>, <thaumicwands:item_wand_rod:7>, <ore:plateGold>]
        ]
    ]
};

static HiddenShapedRecipes as IIngredient[][][][IItemStack] = {

};


/*
    Mirrored Recipes
*/
static MirroredRecipes as IIngredient[][][][IItemStack] = {

};

/*
    Shapeless Recipes
*/
static ShapelessRecipes as IIngredient[][][IItemStack] = {

};

static HiddenShapelessRecipes as IIngredient[][][IItemStack] = {

};

function Init() {
    // Adjust all the recipes
    Util.RemoveRecipes(RecipesToRemove);
	Util.RemoveFurnace(FurnaceRecipesToRemove);
	Util.AddShaped(ShapedRecipes, false, false);
    Util.AddShaped(HiddenShapedRecipes, false, true);
    Util.AddShaped(MirroredRecipes, true, false);
    Util.AddShapeless(ShapelessRecipes, false);
    Util.AddShapeless(HiddenShapelessRecipes, true);
}
