import crafttweaker.item.IIngredient;
import mods.zenstages.ZenStager;

import scripts.crafttweaker.staticstages.Stage_Disabled;
import scripts.crafttweaker.staticstages.MainStage_Thaumcraft;
import scripts.crafttweaker.staticstages.MainStage_Engineering;
import scripts.crafttweaker.staticstages.MainStage_Expansion;
import scripts.crafttweaker.staticstages.MainStage_Automation;

static StagedItems as IIngredient[][string] = {
    Stage_Disabled.stage:
    [
        <thermalfoundation:coin>
    ],

    MainStage_Expansion.stage:
    [
        <thermalfoundation:upgrade>,
        <thermalfoundation:security>,
        <thermalfoundation:diagram_redprint>,
        <thermalfoundation:wrench>,
        <thermalfoundation:meter>,
        <thermalfoundation:fertilizer>,
        <thermalfoundation:material:512>,
        <thermalfoundation:material:513>,
        <thermalfoundation:material:515>,
        <thermalfoundation:material:640>,
        <thermalfoundation:material:656>,
        <thermalfoundation:material:657>,
        <thermalfoundation:material:832>,
        <thermalfoundation:material:833>,
    ]
};

static StagedItemOverrides as IIngredient[][string] = {
    MainStage_Thaumcraft.stage:
    [
        <thermalfoundation:tome_lexicon>
    ]
};

function Init() {
    for StageName, Items in StagedItems {
		ZenStager.getStage(StageName).addIngredients(Items);
	}
}

function InitOverrides() {
    var ModId as string = StagedItemOverrides.entrySet[0].value[0].items[0].definition.owner;
	var ModStage as string = scripts.crafttweaker.staging.modid.ContainsMod(ModId);
    var bModWasStaged as bool = ModStage != "";

    for StageName, Items in StagedItemOverrides {
        if (bModWasStaged && StageName != ModStage)
        {
            ZenStager.addModItemOverrides(ModId, Items);
        }

        for Item in Items {
		    ZenStager.getStage(StageName).addIngredientOverride(Item);
        }
	}
}