#priority -100

import mods.zenstages.ZenStager;

// Init Staging
scripts.crafttweaker.tweaks.tweaks.Init();
scripts.crafttweaker.recipes.recipes.Init();
scripts.crafttweaker.staging.staging.Init();
scripts.crafttweaker.recipes.recipes.InitOverrides();
scripts.crafttweaker.staging.staging.InitOverrides();
ZenStager.buildAll();
ZenStager.checkConflicts();