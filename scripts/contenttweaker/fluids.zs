#loader contenttweaker

import mods.contenttweaker.Color;
import mods.contenttweaker.Fluid;
import mods.contenttweaker.VanillaFactory;

// add liquid mithril
var liquidMithril = VanillaFactory.createFluid("mithril", Color.fromHex("87d0ed"));
liquidMithril.luminosity = 13;
liquidMithril.density = 2000;
liquidMithril.viscosity = 10000;
liquidMithril.temperature = 769;
liquidMithril.register();