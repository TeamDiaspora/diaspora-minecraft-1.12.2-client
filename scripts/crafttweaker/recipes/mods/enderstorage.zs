import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import mods.zenstages.Utils;

/*
    Recipe Removals
*/
static RecipesToRemove as IItemStack[] = [
    <minecraft:ender_chest>,
    <enderstorage:ender_storage>
];

static FurnaceRecipesToRemove as IIngredient[] = [

];

/*
    Shaped Recipes
*/
static ShapedRecipes as IIngredient[][][][IItemStack] = {
    <enderstorage:ender_storage>:
    [
        [
            [<ore:rodBlaze>, <minecraft:wool>, <ore:rodBlaze>],
            [<thaumcraft:mechanism_simple>, <minecraft:ender_chest>, <thaumcraft:mechanism_simple>],
            [<ore:rodBlaze>, <minecraft:obsidian>, <ore:rodBlaze>]
        ]
    ],

    <minecraft:ender_chest>:
    [
        [
            [<abyssalcraft:cingot>, <minecraft:obsidian>, <abyssalcraft:cingot>],
            [<minecraft:obsidian>, <minecraft:ender_eye>, <minecraft:obsidian>],
            [<abyssalcraft:cingot>, <minecraft:obsidian>, <abyssalcraft:cingot>]
        ]
    ]
};

static HiddenShapedRecipes as IIngredient[][][][IItemStack] = {

};


/*
    Mirrored Recipes
*/
static MirroredRecipes as IIngredient[][][][IItemStack] = {

};

/*
    Shapeless Recipes
*/
static ShapelessRecipes as IIngredient[][][IItemStack] = {

};

static HiddenShapelessRecipes as IIngredient[][][IItemStack] = {

};

function Init() {
    // Adjust all the recipes
    Util.RemoveRecipes(RecipesToRemove);
	Util.RemoveFurnace(FurnaceRecipesToRemove);
	Util.AddShaped(ShapedRecipes, false, false);
    Util.AddShaped(HiddenShapedRecipes, false, true);
    Util.AddShaped(MirroredRecipes, true, false);
    Util.AddShapeless(ShapelessRecipes, false);
    Util.AddShapeless(HiddenShapelessRecipes, true);
}
