import mods.nuclearcraft.radiation as Radiation;

import mods.zenstages.Stage;
import scripts.crafttweaker.staticstages.MainStage_Engineering;

function Init() {
    // Make players immune to radiation until they reach an appropriate stage
    Radiation.setRadiationImmunityGameStages(true, [MainStage_Engineering.stage]);
}