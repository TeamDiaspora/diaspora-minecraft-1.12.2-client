import crafttweaker.item.IIngredient;

import mods.zenstages.ZenStager;

import scripts.crafttweaker.staticstages.MainStage_Wilderness;
import scripts.crafttweaker.staticstages.MainStage_Settling;
import scripts.crafttweaker.staticstages.MainStage_Thaumcraft;
import scripts.crafttweaker.staticstages.MainStage_Twilight;
import scripts.crafttweaker.staticstages.MainStage_Astral;
import scripts.crafttweaker.staticstages.MainStage_Transition;
import scripts.crafttweaker.staticstages.MainStage_Engineering;
import scripts.crafttweaker.staticstages.MainStage_Expansion;
import scripts.crafttweaker.staticstages.MainStage_Automation;

import scripts.crafttweaker.staticstages.SubStage_Abyssal;

static StagedItems as string[][string] = {
    MainStage_Thaumcraft.stage: [
        "lycanitesmobs:chupacabra",
        "thermalfoundation:blitz",
        "minecraft:wither_skeleton",
        "minecraft:zombie_pigman",
        "minecraft:enderman",
        "minecraft:blaze",
        "minecraft:ghast",
    ],

    SubStage_Abyssal.stage: [
        "lycanitesmobs:jengu",
        "lycanitesmobs:tarantula",
        "lycanitesmobs:ettin",
        "lycanitesmobs:remobra",
        "lycanitesmobs:afrit",
        "lycanitesmobs:arix",
        "lycanitesmobs:aglebemu",
        "lycanitesmobs:cephignis",
        "lycanitesmobs:aspid",
        "lycanitesmobs:geken",
        "lycanitesmobs:strider",
        "lycanitesmobs:cinder",
        "lycanitesmobs:eechetik",
        "lycanitesmobs:salamander",
        "lycanitesmobs:lobber",
        "lycanitesmobs:gnekk",
        "lycanitesmobs:epion",
        "lycanitesmobs:xaphan",
        "lycanitesmobs:tremor",
        "lycanitesmobs:triffid",
        "lycanitesmobs:geist",
        "lycanitesmobs:necrovore",
        "lycanitesmobs:eyewig",
        "lycanitesmobs:lurker",
        "abyssalcraft:abyssalzombie",
        "abyssalcraft:lessershoggoth",
        "abyssalcraft:depthsghoul",
        "minecraft:creeper",
        "minecraft:witch",
    ]
};

function Init() {
	for Stage, Items in StagedItems {
		ZenStager.getStage(Stage).addMobs(Items);
	}
}