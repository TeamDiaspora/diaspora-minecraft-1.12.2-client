import mods.contenttweaker.tconstruct.MaterialBuilder;

function Init() {
    # add Mithril material
    val MithrilMaterial = MaterialBuilder.create("mithril_mat");
    MithrilMaterial.addItem(<dungeontactics:mithril_ingot>, 1, 144);
    MithrilMaterial.addItem(<dungeontactics:mithril_block>, 9, 1296);
    MithrilMaterial.addHeadMaterialStats(650, 9, 7, 4);
    MithrilMaterial.addHandleMaterialStats(0.85, 200);
    MithrilMaterial.addExtraMaterialStats(50);
    MithrilMaterial.addBowMaterialStats(2.7, 2.2, 11);
    MithrilMaterial.addMaterialTrait("unnatural");
    MithrilMaterial.addMaterialTrait("lightweight");
    MithrilMaterial.castable = true;
    MithrilMaterial.craftable = true;
    MithrilMaterial.liquid = <liquid:mithril>;
    MithrilMaterial.color = 0x87d0ed;
    MithrilMaterial.representativeItem = <dungeontactics:mithril_ingot>;
    MithrilMaterial.itemLocalizer = function(Material, ItemName) { return "Mithril " + ItemName; };
    MithrilMaterial.localizedName = "Mithril";
    MithrilMaterial.register();
}