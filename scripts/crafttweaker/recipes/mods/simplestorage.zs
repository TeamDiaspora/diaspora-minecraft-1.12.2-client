import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import mods.zenstages.Utils;

/*
    Recipe Removals
*/
static RecipesToRemove as IItemStack[] = [
    <storagenetwork:remote>,
    <storagenetwork:remote:3>,
    <storagenetwork:remote:2>,
    <storagenetwork:remote:1>
];

static FurnaceRecipesToRemove as IIngredient[] = [

];

/*
    Shaped Recipes
*/
static ShapedRecipes as IIngredient[][][][IItemStack] = {
    <storagenetwork:remote>:
    [
        [
            [<thaumcraft:ingot:2>, <storagenetwork:im_kabel>, <thaumcraft:ingot:2>],
            [<minecraft:ender_pearl>, <storagenetwork:request>, <minecraft:ender_pearl>],
            [<thaumcraft:ingot:2>, <storagenetwork:ex_kabel>, <thaumcraft:ingot:2>]
        ]
    ],
    <storagenetwork:remote:3>:
    [
        [
            [<thaumcraft:plate:2>, <thaumcraft:vis_resonator>, <thaumcraft:plate:2>],
            [<storagenetwork:inventory>, <thaumcraft:hand_mirror>, <storagenetwork:storage_kabel>],
            [<thaumcraft:plate:2>, <thaumcraft:mechanism_complex>, <thaumcraft:plate:2>]
        ]
    ],
    <storagenetwork:remote:2>:
    [
        [
            [<abyssalcraft:ethaxiumingot>, <abyssalcraft:tieredenergyrelay:3>, <abyssalcraft:ethaxiumingot>],
            [<abyssalcraft:oc>, <storagenetwork:remote:3>, <abyssalcraft:oc>],
            [<abyssalcraft:ethaxiumingot>, <abyssalcraft:tieredenergycontainer:3>, <abyssalcraft:ethaxiumingot>]
        ]
    ],
    <storagenetwork:remote:1>:
    [
        [
            [<astralsorcery:itemcraftingcomponent:1>, <thaumicaugmentation:rift_mover_input>, <astralsorcery:itemcraftingcomponent:1>],
            [<thaumicaugmentation:impetus_mirror>, <storagenetwork:remote:2>, <thaumicaugmentation:impetus_mirror>],
            [<astralsorcery:itemcraftingcomponent:1>, <thaumicaugmentation:rift_mover_output>, <astralsorcery:itemcraftingcomponent:1>]
        ]
    ]
};

static HiddenShapedRecipes as IIngredient[][][][IItemStack] = {

};


/*
    Mirrored Recipes
*/
static MirroredRecipes as IIngredient[][][][IItemStack] = {

};

/*
    Shapeless Recipes
*/
static ShapelessRecipes as IIngredient[][][IItemStack] = {

};

static HiddenShapelessRecipes as IIngredient[][][IItemStack] = {

};

function Init() {
    // Adjust all the recipes
    Util.RemoveRecipes(RecipesToRemove);
	Util.RemoveFurnace(FurnaceRecipesToRemove);
	Util.AddShaped(ShapedRecipes, false, false);
    Util.AddShaped(HiddenShapedRecipes, false, true);
    Util.AddShaped(MirroredRecipes, true, false);
    Util.AddShapeless(ShapelessRecipes, false);
    Util.AddShapeless(HiddenShapelessRecipes, true);
}
