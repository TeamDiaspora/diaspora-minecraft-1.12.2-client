#loader mixin

import native.tauri.dev.jsg.JSG;
import native.tauri.dev.jsg.tileentity.stargate.StargateOrlinBaseTile;
import native.tauri.dev.jsg.tileentity.stargate.StargateAbstractBaseTile;
import native.tauri.dev.jsg.stargate.network.StargateNetwork;
import native.tauri.dev.jsg.stargate.network.StargatePos;
import native.tauri.dev.jsg.stargate.network.StargateAddress;
import native.tauri.dev.jsg.stargate.network.StargateAddressDynamic;
import native.tauri.dev.jsg.worldgen.structures.EnumStructures;
import native.tauri.dev.jsg.worldgen.util.GeneratedStargate;
import native.tauri.dev.jsg.worldgen.structures.stargate.StargateGenerator;
import native.net.minecraft.world.World;
import native.net.minecraft.world.DimensionType;
import native.net.minecraft.util.math.BlockPos;

import mixin.CallbackInfo;

#mixin Mixin
#{targets: "tauri.dev.jsg.tileentity.stargate.StargateOrlinBaseTile"}
zenClass MixinStargateOrlinBaseTile {
    #mixin Shadow
    var canNotGenerate as bool;

    var generatedAddress as StargateAddress;

    var generatedGate as GeneratedStargate;

    #mixin Overwrite
    function canAcceptConnectionFrom(targetGatePos as StargatePos) as bool {
        return this0.canAcceptConnectionFrom(targetGatePos) && !this0.isBroken() && targetGatePos.dimensionID == 111;
    }

    #mixin Redirect
    #{
    #   method: "beginOpening",
    #   at: {
    #       value: "INVOKE",
    #       target: "Ltauri/dev/jsg/stargate/network/StargateNetwork;generateNetherGate(Ltauri/dev/jsg/stargate/network/StargateNetwork;Lnet/minecraft/world/World;Lnet/minecraft/util/math/BlockPos;)Ltauri/dev/jsg/worldgen/util/GeneratedStargate;"
    #   }
    #}
    function generateStargate(network as StargateNetwork, world as World, pos as BlockPos) as GeneratedStargate {
        if (generatedGate != null)
        {
            JSG.info("Orlin gate already generated stargate in dimension 111");
            return generatedGate;
        }

        generatedGate = StargateGenerator.mystPageGeneration(world, EnumStructures.INTERNAL_MW, 111, pos);
        if (generatedGate != null)
        {
            generatedAddress = generatedGate.address;
            JSG.info("Orlin gate generated stargate in dimension 111, address " + generatedAddress.toString());
        }
        else
        {
            generatedAddress = null;
            JSG.info("Orlin gate failed to generate stargate in dimension 111");
        }

        return generatedGate;
    }

    #mixin Overwrite
    function updateNetherAddress() as void {
        var network as StargateNetwork = this0.getNetwork();
        var world as World = this0.world;

        this0.dialedAddress.clear();

        if (generatedAddress == null || !network.isStargateInNetwork(generatedAddress) || network.getStargate(generatedAddress) == null)
        {
            if (!world.isRemote && world.provider.getDimensionType() == DimensionType.OVERWORLD)
            {
                var stargate as GeneratedStargate = generateStargate(network, world, this0.getPos());
                if (stargate == null)
                {
                    canNotGenerate = true;
                    this0.markDirty();
                    return;
                }
            }
        }

        this0.dialedAddress.addAll(generatedAddress.subList(0, 7));
        this0.dialedAddress.addSymbol(generatedAddress.getSymbolType().getOrigin());
        this0.markDirty();

        JSG.info("Orlin gate dialed address " + this0.dialedAddress.toString());
    }
}