import mods.jei.JEI as JEI;

function Init() {
    // Hide AE2 facades
    JEI.hide(<appliedenergistics2:facade>);
}