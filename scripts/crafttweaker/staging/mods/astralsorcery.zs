import crafttweaker.item.IIngredient;
import mods.zenstages.ZenStager;

import scripts.crafttweaker.staticstages.Stage_Disabled;

static StagedItems as IIngredient[][string] = {

};

static StagedItemOverrides as IIngredient[][string] = {
    Stage_Disabled.stage: [
        <astralsorcery:blockcelestialgateway>
    ]
};

function Init() {
    for StageName, Items in StagedItems {
		ZenStager.getStage(StageName).addIngredients(Items);
	}
}

function InitOverrides() {
    var ModId as string = StagedItemOverrides.entrySet[0].value[0].items[0].definition.owner;
	var ModStage as string = scripts.crafttweaker.staging.modid.ContainsMod(ModId);
    var bModWasStaged as bool = ModStage != "";

    for StageName, Items in StagedItemOverrides {
        if (bModWasStaged && StageName != ModStage)
        {
            ZenStager.addModItemOverrides(ModId, Items);
        }

        for Item in Items {
		    ZenStager.getStage(StageName).addIngredientOverride(Item);
        }
	}
}