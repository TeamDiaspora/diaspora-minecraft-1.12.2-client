#priority -10

function Init() {
    scripts.crafttweaker.tweaks.oredict.Init();
    scripts.crafttweaker.tweaks.mods.bonsai.Init();
    scripts.crafttweaker.tweaks.mods.jei.Init();
    scripts.crafttweaker.tweaks.mods.nuclearcraft.Init();
    scripts.crafttweaker.tweaks.mods.thaumcraft.Init();
    scripts.crafttweaker.tweaks.mods.tinkers.Init();
}