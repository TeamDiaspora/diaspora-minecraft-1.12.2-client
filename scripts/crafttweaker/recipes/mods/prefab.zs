import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import mods.zenstages.Utils;

/*
    Recipe Removals
*/
static RecipesToRemove as IItemStack[] = [
    <prefab:item_bundle_of_timber>,
    <prefab:block_compressed_stone>
];

static RecipesToRemove2 as string[] = [
    "prefab:compressed_stone_to_stone",
];

static FurnaceRecipesToRemove as IIngredient[] = [

];

/*
    Shaped Recipes
*/
static ShapedRecipes as IIngredient[][][][IItemStack] = {
    <prefab:item_bundle_of_timber>:
    [
        [
            [<ore:plankWood>, <ore:plankWood>, <ore:plankWood>],
            [<ore:plankWood>, <ore:plankWood>, <ore:plankWood>],
            [<ore:plankWood>, <ore:plankWood>, <ore:plankWood>]
        ]
    ],

    <prefab:block_compressed_stone>:
    [
        [
            [<ore:cobblestone>, <ore:cobblestone>, <ore:cobblestone>],
            [<ore:cobblestone>, <ore:cobblestone>, <ore:cobblestone>],
            [<ore:cobblestone>, <ore:cobblestone>, <ore:cobblestone>]
        ]
    ],
};

static HiddenShapedRecipes as IIngredient[][][][IItemStack] = {

};

/*
    Mirrored Recipes
*/
static MirroredRecipes as IIngredient[][][][IItemStack] = {

};

/*
    Shapeless Recipes
*/
static ShapelessRecipes as IIngredient[][][IItemStack] = {
    <prefab:block_compressed_stone> * 9:
    [
        [
            <prefab:block_double_compressed_stone>
        ]
    ],

    <minecraft:cobblestone> * 9:
    [
        [
            <prefab:block_compressed_stone>
        ]
    ],
};

static HiddenShapelessRecipes as IIngredient[][][IItemStack] = {

};

function Init() {
    Util.RemoveRecipes(RecipesToRemove);
	Util.RemoveFurnace(FurnaceRecipesToRemove);
	Util.AddShaped(ShapedRecipes, false, false);
    Util.AddShaped(HiddenShapedRecipes, false, true);
    Util.AddShaped(MirroredRecipes, true, false);
    Util.AddShapeless(ShapelessRecipes, false);
    Util.AddShapeless(HiddenShapelessRecipes, true);
}
