import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import mods.zenstages.Utils;

/*
    Recipe Removals
*/
static RecipesToRemove as IItemStack[] = [

];

static FurnaceRecipesToRemove as IIngredient[] = [

];

/*
    Shaped Recipes
*/
static ShapedRecipes as IIngredient[][][][IItemStack] = {
    <minecraft:paper>:
    [
        [
            [<notreepunching:grass_fiber>, <notreepunching:grass_fiber>, <notreepunching:grass_fiber>],
            [<notreepunching:grass_fiber>, <notreepunching:grass_fiber>, <notreepunching:grass_fiber>],
            [<notreepunching:grass_fiber>, <notreepunching:grass_fiber>, <notreepunching:grass_fiber>]
        ]
    ],

    <notreepunching:grass_fiber>:
    [
        [
            [<minecraft:wheat_seeds>, <minecraft:wheat_seeds>, <minecraft:wheat_seeds>],
            [<minecraft:wheat_seeds>, <minecraft:wheat_seeds>, <minecraft:wheat_seeds>],
            [<minecraft:wheat_seeds>, <minecraft:wheat_seeds>, <minecraft:wheat_seeds>],
        ]
    ],
};

static HiddenShapedRecipes as IIngredient[][][][IItemStack] = {

};

/*
    Mirrored Recipes
*/
static MirroredRecipes as IIngredient[][][][IItemStack] = {

};

/*
    Shapeless Recipes
*/
static ShapelessRecipes as IIngredient[][][IItemStack] = {

};

static HiddenShapelessRecipes as IIngredient[][][IItemStack] = {

};

function Init() {
    Util.RemoveRecipes(RecipesToRemove);
	Util.RemoveFurnace(FurnaceRecipesToRemove);
	Util.AddShaped(ShapedRecipes, false, false);
    Util.AddShaped(HiddenShapedRecipes, false, true);
    Util.AddShaped(MirroredRecipes, true, false);
    Util.AddShapeless(ShapelessRecipes, false);
    Util.AddShapeless(HiddenShapelessRecipes, true);

    furnace.addRecipe(<thermalfoundation:material:769>, <minecraft:wheat_seeds>);
}
